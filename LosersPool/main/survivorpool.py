from operator import itemgetter
from bs4 import BeautifulSoup
import requests


class SurvivorPool:

    def __init__(self):
        self.schedule = {}
        self.elo = {}
        self.manual_adujstments = {}
        self.team_chances = {}
        self.team_lines = {}
        self.low_score = 100000000
        self.low_perms = []
        self.weeks_to_sim = 7
        self.current_week = 1
        self.save_current_week_scores()
        self.manual_adjustments = {}

    def convert_line_to_dec(self, line):
        if line < 0:
            return abs(line) / (abs(line)+100)
        else:
            return 1-(line / (line+100))

    def save_current_week_scores(self):
        self.team_lines['ARI'] = self.convert_line_to_dec(257)
        self.team_lines['ATL'] = self.convert_line_to_dec(116)
        self.team_lines['BAL'] = self.convert_line_to_dec(-297)
        self.team_lines['BUF'] = self.convert_line_to_dec(285)
        self.team_lines['CAR'] = self.convert_line_to_dec(153)
        self.team_lines['CHI'] = self.convert_line_to_dec(138)
        self.team_lines['CIN'] = self.convert_line_to_dec(152)
        self.team_lines['CLE'] = self.convert_line_to_dec(303)
        self.team_lines['DAL'] = self.convert_line_to_dec(-153)
        self.team_lines['DEN'] = self.convert_line_to_dec(-105)
        self.team_lines['DET'] = self.convert_line_to_dec(-153)
        self.team_lines['GB'] = self.convert_line_to_dec(132)
        self.team_lines['HOU'] = self.convert_line_to_dec(353)
        self.team_lines['IND'] = self.convert_line_to_dec(-375)
        self.team_lines['JAX'] = self.convert_line_to_dec(314)
        self.team_lines['KC'] = self.convert_line_to_dec(-427)
        self.team_lines['LAC'] = self.convert_line_to_dec(-169)
        self.team_lines['LAR'] = self.convert_line_to_dec(137)
        self.team_lines['MIA'] = self.convert_line_to_dec(251)
        self.team_lines['MIN'] = self.convert_line_to_dec(-146)
        self.team_lines['NE'] = self.convert_line_to_dec(-291)
        self.team_lines['NO'] = self.convert_line_to_dec(-171)
        self.team_lines['NYG'] = self.convert_line_to_dec(208)
        self.team_lines['NYJ'] = self.convert_line_to_dec(256)
        self.team_lines['LV'] = self.convert_line_to_dec(-171)
        self.team_lines['PHI'] = self.convert_line_to_dec(-263)
        self.team_lines['PIT'] = self.convert_line_to_dec(-237)
        self.team_lines['SEA'] = self.convert_line_to_dec(-128)
        self.team_lines['SF'] = self.convert_line_to_dec(-299)
        self.team_lines['TB'] = self.convert_line_to_dec(153)
        self.team_lines['TEN'] = self.convert_line_to_dec(-105)
        self.team_lines['WSH'] = self.convert_line_to_dec(229)

    def adjust_preseason(self, elo_num):
        return (int(elo_num)-1500)*.6875 + 1500

    def pre_season_elo(self):
        content = requests.get(
            'https://projects.fivethirtyeight.com/2019-nfl-predictions/')
        soup = BeautifulSoup(content.text, features="lxml")
        #desc = soup.find_all('table', 'playoff-mode')
        desc = soup.find_all('tr', attrs={'data-team': True})
        for obj in desc:
            team = obj.attrs['data-team']
            elo = int(obj.find('td', 'num elo').get_text())
            if elo > 1500:
                elo = elo - int((elo - 1500)/3)
            elif elo < 1500:
                elo = elo + int((1500 - elo)/3)
            else:
                elo = 1500
            if team == 'OAK': team = 'LV'
            self.elo[team] = int(elo)

    def scrape_pinnacle(self):
        content = requests.get('http://www.espn.com/nfl/lines')
        soup = BeautifulSoup(content.text, features="lxml")
        desc = soup.find_all('table', 'tr')
        for obj in desc:
            print(obj.tr.td)
            print(obj.prettify())
            #team_row = obj.find_all()

    def scrape_schedule(self):
        for week in range(1, 18):
            if week is 1:
                content = requests.get('https://www.espn.com/nfl/schedule')
            else:
                url = 'https://www.espn.com/nfl/schedule' + \
                    '/_/week/' + str(week)
                content = requests.get(url)
            self.schedule[week] = []
            soup = BeautifulSoup(content.text, features="lxml")
            away = True
            matchup = {}
            for obj in soup.find_all('abbr'):
                if away:
                    matchup['away'] = obj.get_text()
                    away = False
                else:
                    matchup['home'] = obj.get_text()
                    away = True
                    self.schedule[week].append(matchup)
                    matchup = {}

    def calc_chance(self, h, a):
        eloH = h + 10
        eloA = a
        # positive number favors home team, negative number favors away team
        chanceH = 1/(10**((eloH-eloA)/400)+1)
        return chanceH

    def apply_adjustments(self):
        for key in self.manual_adjustments:
            self.elo[key] += self.manual_adjustments[key]
            print('adjusted ' + key + ' by ' + str(self.manual_adjustments[key]))

    def calc_diff(self, num_weeks):
        for week in self.schedule:
            for matchup in self.schedule[week]:
                home_team = matchup['home']
                away_team = matchup['away']
                if home_team not in self.team_chances:
                    self.team_chances[home_team] = {}
                if away_team not in self.team_chances:
                    self.team_chances[away_team] = {}
                if week is self.current_week:
                    # use self.current_lines
                    home_line = self.team_lines[home_team]
                    away_line = self.team_lines[away_team]
                    self.team_chances[home_team][week] = home_line / (home_line + away_line)
                    self.team_chances[away_team][week] = away_line / (home_line + away_line)
                else:
                    results = self.calc_chance(self.elo[home_team], self.elo[away_team])
                    self.team_chances[home_team][week] = 1 - results
                    self.team_chances[away_team][week] = results

    def check_score(self, s, pe, low):
        if s < low*2:
            # print(pe)
            self.low_score = s
            newPerm = list(pe)
            record = [s, newPerm]
            # print(record)
            self.low_perms.append(record)

    def calcPerm(self, perms, score, week):
        if week is self.weeks_to_sim+1:
            # print(p)
            self.check_score(score, perms, self.low_score)
            #newPerm = list(p)
            #r = [s,newPerm]
            # addLowPerm(r)
        else:
            for team in self.team_chances:
                # print(p)
                # print(lowPerms)
                # print(key)
                match = False
                for x in perms:
                    if x == team:
                        match = True
                        break
                    elif x == '':
                        break
                # for perm in self.low_perms:
                #     for x in range(self.current_week-1, week+1):
                #         if perm[1][x] == perms[x]:
                #             match = True
                #             break
                #     if match:
                #         break
                if not(match) and self.team_chances[team][week] < .48:
                    localP = perms.copy()
                    localP[week-1] = team
                    # print(p)
                    localS = score + self.team_chances[team][week]
                    if localS < .32*(self.weeks_to_sim-self.current_week):
                        self.calcPerm(localP, localS, week+1)

    def sort_and_print(self):
        local_perms = self.low_perms.sort(key=itemgetter(0), reverse=False)
        i = 1
        for y in local_perms:
            if i is 1:
                print(y)
                i += 1

        # i = 1
        # for x in lowPerms:
        #     if i <= 500:
        #         print(x)
        #         i += 1


if __name__ == "__main__":
    my_picks = []
    my_picks.append(['','','','','','','','','','','','','','','','','',])
    my_picks.append(['','','','','','','','','','','','','','','','','',])
    my_picks.append(['','','','','','','','','','','','','','','','','',])
    my_picks.append(['','','','','','','','','','','','','','','','','',])
    my_picks.append(['','','','','','','','','','','','','','','','','',])
    pool = SurvivorPool()
    print('scraping schedule')
    pool.scrape_schedule()
    print('fetching elo')
    pool.pre_season_elo()
    print('applying adjustments')
    pool.apply_adjustments()
    print('calculating chances')
    pool.calc_diff(17)
    end_perms = []
    for pick in my_picks:
        print('----------new pick----------')
        team_scores = {}
        for team in pool.team_chances:
            print('permutations for ' + team)
            chance = pool.team_chances[team][pool.current_week] 
            broken = False
            for x in pick:
                if x == team:
                    broken = True
                    break
                elif x == '':
                    break
            if broken:
                continue
            if chance < .48:
                local_pick = pick.copy()
                local_pick[pool.current_week-1] = team
                pool.calcPerm(local_pick, chance, pool.current_week+1)
        if pool.low_perms:
            perms_to_print = []
            sorted_perms = sorted(pool.low_perms, key=itemgetter(0))
            for perm in sorted_perms:
                if not end_perms:
                    end_perms.append(perm)
                    break
                else:
                    broken = False
                    for saved_perm in end_perms:
                        for i in range(pool.current_week,min((int(pool.weeks_to_sim)-2),pool.current_week+3)):
                            left = perm[1][i-1]
                            right = saved_perm[1][i-1] 
                            if left == '' or right == '':
                                break
                            if left == right:
                                if pool.team_chances[left][i] > .10:
                                    broken = True
                                    break
                        if broken:
                            break
                    if not broken:
                        end_perms.append(perm)
                        break
            pool.low_perms = []
    for x in sorted(end_perms, key=itemgetter(0)):
        print(x)
        #print('--------------------next pick------------------------')

