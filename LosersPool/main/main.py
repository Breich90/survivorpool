from operator import itemgetter
from bs4 import BeautifulSoup
import requests

#url = "www.vegasinsider.com/nfl/odds/las-vegas/money/"

#r = requests.get("http://" + url)

#data = r.text

#soup = BeautifulSoup(data, "html.parser")

#lines = soup.find_all("table", class_="frodds-data-tbl") #, attrs={"ng-if": "participant.Name != undefined"}

#for x in soup.stripped_strings:
#    print(x)

        

#for x in lines:
     #print(x)
                     
                     

#returns dictionary of team Elo, keys of team abbreviation, indices 1-17 are weeks.  values represent chances to win.  value of 2 equals a bye, must check for this. 
def main():
    teamDict = {'DEN':[1644,2,2,171,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
                   'SEA':[1623,2,2,2,2,2,2,105,2,2,2,2,2,2,2,2,2,2],
                   'CAR':[1556,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
                   'KC':[1644,2,.488,2,159,2,2,2,2,2,2,2,2,2,2,2,2,2],
                   'ARI':[1568,2,2,2,2,2,2,2,147,2,2,2,2,2,2,2,2,2],
                   'NE':[1683,2,2,.5,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
                   'PIT':[1566,2,2,2,2,2,2,270,2,2,2,2,2,2,2,2,2,2],
                   'GB':[1566,2,2,2,2,2,2,2,152,2,2,2,2,2,2,2,2,2],
                   'CIN':[1525,2,.405,2,2,100,350,2,2,2,2,2,2,2,2,2,2,2],
                   'MIN':[1585,2,2,282,2,2,2,2,2,2,2,2,2,2,2,2,2,2],
                   'NYJ':[1494,2,2,109,2,305,320,2,2,177,2,2,2,2,2,2,2,2],
                   'BUF':[1552,2,2,171,165,135,2,2,231,270,2,2,2,2,2,2,2,2],
                   'HOU':[1527,2,2,.5,2,251,2,320,2,2,2,2,2,2,2,2,2,2],
                   'DET':[1495,2,2,268,2,176,2,2,120,233,2,2,2,2,2,2,2,2],
                   'WSH':[1509,2,2,156,2,180,124,113,138,2,2,2,2,2,2,2,2,2],
                   'PHI':[1555,2,.375,152,2,2,2,144,190,128,2,2,2,2,2,2,2,2],
                   'ATL':[1553,2,.350,144,141,195,250,2,2,2,2,2,2,2,2,2,2,2],
                   'IND':[1453,2,2,2,2,2,152,161,126,290,2,2,2,2,2,2,2,2],
                   'LA':[1459,2,.285,185,380,2,161,127,2,156,2,2,2,2,2,2,2,2],
                   'BAL':[1449,2,.32,.5,2,2,135,117,2,2,2,2,2,2,2,2,2,2],
                   'NYG':[1488,2,2,2,181,292,2,2,2,2,2,2,2,2,2,2,2,2],
                   'NO':[1462,2,.357,2,185,2,2,218,124,2,2,2,2,2,2,2,2,2],
                   'OAK':[1503,2,2,.465,170,2,2,117,2,2,2,2,2,2,2,2,2,2],
                   'DAL':[1582,2,.39,2,2,118,199,2,2,2,2,2,2,2,2,2,2,2],
                   'CHI':[1410,2,2,263,159,195,2,343,180,2,2,2,2,2,2,2,2,2],
                   'MIA':[1449,2,.296,2,320,2,300,127,2,2,2,2,2,2,2,2,2,2],
                   'SD':[1469,2,2,111,2,175,161,218,200,2,2,2,2,2,2,2,2,2],
                   'SF':[1358,2,.159,355,109,2,293,111,2,195,2,2,2,2,2,2,2,2],
                   'TB':[1432,2,.241,2,168,192,2,2,2,170,2,2,2,2,2,2,2,2],
                   'CLE':[1300,2,2,340,321,2,289,420,128,280,2,2,2,2,2,2,2,2],
                   'JAX':[1329,2,.420,105,117,2,134,2,2,300,2,2,2,2,2,2,2,2],
                   'TEN':[1373,2,.30,107,185,2,168,2,2,175,2,2,2,2,2,2,2,2],
                   }
    
    manualAdjustments = {}    
    
    #returns dictionary of weeks, each week is an array of game matchups [h,a]
    def getSchedule():
        schedule = {1: [['DEN','CAR'],['BAL','BUF'],['HOU','CHI'],['NYJ','CIN'],['PHI','CLE'],['JAX','GB'],['TEN','MIN'],['NO','OAK'],['KC','SD'],['ATL','TB'],['SEA','MIA'],['IND','DET'],['DAL','NYG'],['ARI','NE'],['WSH','PIT'],['SF','LA']],
                    2: [['BUF','NYJ'],['CLE','BAL'],['PIT','CIN'],['WSH','DAL'],['HOU','KC'],['NE','MIA'],['NYG','NO'],['CAR','SF'],['DET','TEN'],['LA','SEA'],['ARI','TB'],['OAK','ATL'],['DEN','IND'],['SD','JAX'],['MIN','GB'],['CHI','PHI']],
                    3: [['NE','HOU'],['BUF','ARI'],['JAX','BAL'],['MIA','CLE'],['CIN','DEN'],['GB','DET'],['CAR','MIN'],['TEN','OAK'],['NYG','WSH'],['TB','LA'],['SEA','SF'],['KC','NYJ'],['PHI','PIT'],['IND','SD'],['DAL','CHI'],['NO','ATL']],
                    4: [['CIN','MIA'],['JAX','IND'],['NE','BUF'],['ATL','CAR'],['WSH','CLE'],['CHI','DET'],['BAL','OAK'],['NYJ','SEA'],['HOU','TEN'],['TB','DEN'],['SF','DAL'],['ARI','LA'],['SD','NO'],['PIT','KC'],['MIN','NYG']],
                    5: [['SF','ARI'],['IND','CHI'],['MIN','HOU'],['CLE','NE'],['PIT','NYJ'],['DET','PHI'],['MIA','TEN'],['BAL','WSH'],['DEN','ATL'],['LA','BUF'],['DAL','CIN'],['OAK','SD'],['GB','NYG'],['CAR','TB']],
                    6: [['SD','DEN'],['NYG','BAL'],['NO','CAR'],['NE','CIN'],['TEN','CLE'],['CHI','JAX'],['DET','LA'],['WSH','PHI'],['MIA','PIT'],['BUF','SF'],['OAK','KC'],['SEA','ATL'],['GB','DAL'],['HOU','IND'],['ARI','NYJ']],
                    7: [['GB','CHI'],['LA','NYG'],['NYG','BAL'],['MIA','BUF'],['CIN','CLE'],['TEN','IND'],['PHI','MIN'],['KC','NO'],['JAX','OAK'],['DET','WSH'],['ATL','SD'],['SF','TB'],['PIT','NE'],['ARI','SEA'],['DEN','HOU']],
                    8: [['TEN','JAX'],['CIN','WSH'],['HOU','DET'],['ATL','GB'],['IND','KC'],['BUF','NE'],['CLE','NYJ'],['TB','OAK'],['NO','SEA'],['DEN','SD'],['CAR','ARI'],['DAL','PHI'],['CHI','MIN']],
                    9: [['TB','ATL'],['CLE','DAL'],['MIN','DET'],['KC','JAX'],['MIA','NYJ'],['NYG','PHI'],['BAL','PIT'],['LA','CAR'],['SF','NO'],['GB','IND'],['SD','TEN'],['OAK','DEN'],['SEA','BUF']],
                    10: [['BAL','CLE'],['PHI','ATL'],['TB','CHI'],['NO','DEN'],['TEN','GB'],['JAX','HOU'],['CAR','KC'],['NYJ','LA'],['WSH','MIN'],['SD','MIA'],['PIT','DAL'],['ARI','SF'],['NE','SEA'],['NYG','CIN']],
                    11: [['CAR','NO'],['MIN','ARI'],['DAL','BAL'],['CIN','BUF'],['NYG','CHI'],['DET','JAX'],['CLE','PIT'],['KC','TB'],['IND','TEN'],['LA','MIA'],['SF','NE'],['SEA','PHI'],['WSH','GB'],['OAK','HOU']],
                    12: [['DET','MIN'],['DAL','WSH'],['IND','PIT'],['ATL','ARI'],['BAL','CIN'],['BUF','JAX'],['NO','LA'],['CLE','NYG'],['HOU','SD'],['MIA','SF'],['CHI','TEN'],['TB','SEA'],['OAK','CAR'],['DEN','KC'],['NYJ','NE'],['PHI','GB']],
                    13: [['MIN','DAL'],['JAX','DEN'],['NO','DET'],['GB','HOU'],['ATL','KC'],['NE','LA'],['BAL','MIA'],['CIN','PHI'],['CHI','SF'],['OAK','BUF'],['PIT','NYG'],['SD','TB'],['ARI','WSH'],['SEA','CAR'],['NYJ','IND']],
                    14: [['KC','OAK'],['MIA','ARI'],['DET','CHI'],['CLE','CIN'],['TEN','DEN'],['IND','HOU'],['JAX','MIN'],['TB','NO'],['BUF','PIT'],['CAR','SD'],['PHI','WSH'],['SF','NYJ'],['LA','ATL'],['GB','SEA'],['NYG','DAL'],['NE','BAL']],
                    15: [['SEA','LA'],['NYJ','MIA'],['BUF','CLE'],['NYG','DET'],['CHI','GB'],['MIN','IND'],['HOU','JAX'],['BAL','PHI'],['DAL','TB'],['KC','TEN'],['ARI','NO'],['ATL','SF'],['DEN','NE'],['SD','OAK'],['CIN','PIT'],['WSH','CAR']],
                    16: [['PHI','NYG'],['CAR','ATL'],['BUF','MIA'],['GB','MIN'],['NE','NYJ'],['CLE','SD'],['NO','TB'],['JAX','TEN'],['CHI','WSH'],['OAK','IND'],['SEA','ARI'],['LA','SF'],['HOU','CIN'],['PIT','BAL'],['KC','DEN'],['DAL','DET']],
                    17: [['CIN','BAL'],['NYJ','BUF'],['TB','CAR'],['MIN','CHI'],['PIT','CLE'],['PHI','DAL'],['DET','GB'],['TEN','HOU'],['IND','JAX'],['MIA','NE'],['ATL','NO'],['WSH','NYG'],['LA','ARI'],['SD','KC'],['DEN','OAK'],['SF','SEA']],
                    }
        return schedule
    
    def calcChance(h,a):
        eloH = h + 65
        eloA = a
        #positive number favors home team, negative number favors away team
        chanceH = 1/(10**((eloH-eloA)/400)+1) 
        return [1-chanceH,chanceH]
    
    def applyAdjustments():
        for key in manualAdjustments:
            teamDict[key][0] = teamDict[key][0] - manualAdjustments[key]
    
    def calcDiff():
        s = getSchedule()
        i = 1
        while i <= numWeeks:
            #print(i)
            for x in s[i]:
                if teamDict[x[0]][i] == 2 and teamDict[x[1]][i] == 2:
                    results = calcChance(teamDict[x[0]][0],teamDict[x[1]][0])
                    #print(results)
                    teamDict[x[0]][i] = results[0]
                    teamDict[x[1]][i] = results[1] 
                elif teamDict[x[0]][i] > 2:
                    teamDict[x[0]][i] = 100/(teamDict[x[0]][i] + 100) 
                elif teamDict[x[1]][i] > 2:
                    teamDict[x[1]][i] = 100/(teamDict[x[1]][i] + 100)       
            #print(teamDict['CHI'])      
            i += 1
            
    #calcDiff()
    
    def getLowScore():
        return lowScore
    
    def setLowScore(s):
        global lowScore
        lowScore = s
        
    def addLowPerm(r):
        global lowPerms
        #print(lowPerms)
        lowPerms.append(r)
        #print(lowPerms)
    
    def checkScore(s,pe,low):
        if s < low*1.1:
            #print(pe)
            setLowScore(s)
            newPerm = list(pe)
            record = [s,newPerm]
            #print(record)
            addLowPerm(record)
    
    def calcPerm(p,s,n):
        if n == numWeeks+1:
            #print(p)
            checkScore(s,p,getLowScore())
            #newPerm = list(p)
            #r = [s,newPerm]
            #addLowPerm(r)                
        else:
            for key in teamDict:
                #print(p)
                #print(lowPerms)
                #print(key)
                match = 0
                for x in p:
                    if x == key: match = 1
                if match == 0 and teamDict[key][n] < .48:
                    localP = p
                    localP[n-1] = key
                    #print(p)
                    localS = s + teamDict[key][n]
                    if localS < .32*(numWeeks-currentWeek):
                        calcPerm(localP,localS,n+1)
    
    global lowScore          
    lowScore = 100000000       
    global lowPerms    
    lowPerms = list()
    global numWeeks
    numWeeks = 17
    currentWeek = 9
    calcDiff()

    def sortAndPrint():
        global lowPerms
        lowPerms.sort(key=itemgetter(0), reverse=False)
        i = 1
        for y in lowPerms:
            if i == 1:
                print(y)
                i += 1
        lowPerms = list()
            
    for k,value in teamDict.items():
        #print(value)
        print(k)
        print(value[currentWeek])
        if value[currentWeek] < .46:
            #if k != 'CHI'and k != 'IND': calcPerm(['CHI','IND',k,'','','','','','','','','','','','','','',],value[currentWeek],currentWeek+1)
            #sortAndPrint()
            #if k != 'MIA' and k != 'SF' and k != 'DET': calcPerm(['MIA','SF','DET',k,'','','','','','','','','','','','','',],value[currentWeek],currentWeek+1)
            #sortAndPrint()
        #Brooks
            if k!= 'BUF' and k!= 'HOU' and k!= 'NYJ' and k!= 'NYG' and k != 'WSH' and k != 'MIA' and k != 'SF' and k != 'CLE': 
                calcPerm(['WSH','MIA','SF','CLE','NYG','NYJ','HOU','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                print('*')
                sortAndPrint()
        #Artomatic  
            if k!= 'BUF' and k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'SD' and k != 'SF' and k != 'CHI' and k != 'KC': 
                calcPerm(['SD','SF','CHI','KC','NYJ','CIN','PIT','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #donchan1    
            #if k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'MIA' and k != 'SF' and k != 'DET' and k != 'CLE': calcPerm(['DET','MIA','SF','CLE','NYJ','CIN','PIT',k,'','','','','','','','','',],value[currentWeek],currentWeek+1)
            #sortAndPrint()
        #Flash    
            if k!= 'BUF' and k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'TB' and k != 'MIA' and k != 'SF' and k != 'CLE': 
                calcPerm(['TB','MIA','SF','CLE','NYJ','CIN','PIT','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #John Parker    
            if k!= 'BUF' and k!= 'NO' and k!= 'CIN' and k!= 'NYJ' and k != 'TB' and k != 'MIA' and k != 'DET' and k != 'CLE': 
                calcPerm(['TB','MIA','DET','CLE','NYJ','CIN','NO','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #Kyle L    
            if k!= 'BUF' and k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'JAX' and k != 'MIA' and k != 'TB' and k != 'CHI': 
                calcPerm(['JAX','MIA','TB','CHI','NYJ','CIN','PIT','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #Mike D #2
            if k!= 'BUF' and k!= 'HOU' and k!= 'ATL' and k!= 'NYJ' and k != 'CHI' and k != 'MIA' and k != 'SF' and k != 'CLE': 
                calcPerm(['CHI','MIA','SF','CLE','NYJ','ATL','HOU','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #NYC
            #if k!= 'NYJ' and k!= 'CLE' and k != 'TB' and k != 'MIA' and k != 'DET' and k != 'KC': calcPerm(['TB','MIA','DET','KC','CLE','NYJ',k,'','','','','','','','','','',],value[currentWeek],currentWeek+1)
            #sortAndPrint()
        #rockhound    
            if k!= 'BUF' and k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'SD' and k != 'DET' and k != 'SF' and k != 'KC': 
                calcPerm(['SD','DET','SF','CLE','NYJ','CIN','PIT','BUF',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #Stump #2    
            if k!= 'PHI' and k!= 'CHI' and k!= 'CIN' and k!= 'NYJ' and k != 'SD' and k != 'TB' and k != 'SF' and k != 'CLE': 
                calcPerm(['SD','TB','SF','CLE','NYJ','CIN','CHI','PHI',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #Super Stinky    
            if k!= 'PHI' and k!= 'PIT' and k!= 'CIN' and k!= 'NYJ' and k != 'CHI' and k != 'IND' and k != 'SF' and k != 'CLE': 
                calcPerm(['CHI','IND','SF','CLE','NYJ','CIN','PIT','PHI',k,'','','','','','','','',],value[currentWeek],currentWeek+1)
                sortAndPrint()
        #VictoryLD #2
            #if k!= 'JAX' and k!= 'CIN' and k!= 'CLE' and k != 'SD' and k != 'MIA' and k != 'DET' and k != 'TEN': calcPerm(['SD','MIA','DET','TEN','CLE','CIN','JAX',k,'','','','','','','','','',],value[currentWeek],currentWeek+1)
            #sortAndPrint()
    #calcPerm(['DEN','','','','','','','','','','','','','','','','',],0.6,2)
    #print(lowScore)
    #lowPerms.sort(key=itemgetter(0), reverse=False)
    
    i = 1
    for x in lowPerms:
        if i <= 500:
            print(x)
            i += 1
    
main()
        
        